/*1. El gobierno del estado de M�xico desea reforestar un bosque que mide determinado n�mero de hect�reas. Si la superficie del terreno excede a 1 mill�n de metros cuadrados, 
entonces decidir� sembrar de la sig. manera:
porcentaje de la superficie del bosque Tipo de �rbol
70% pino
20% oyamel
10% cedro
Si la superficie del terreno es menor o igual a un mill�n de metros cuadrados, entonces decidir� sembrar de la sig. manera:
Porcentaje de la superficie del bosque Tipo de �rbol
50% pino
30% oyamel
20% cedro
El gobierno desea saber el n�mero de pinos, oyameles y cedros que tendr� que sembrar en el bosque, si se sabe que en 10 metros cuadrados caben 8 pinos, en 15 metros cuadrados caben 15
oyameles y en 18 metros cuadrados caben 10 cedros. Tambi�n se sabe que una hect�rea equivale a 10 mil metros cuadrados.*/

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <locale.h>

using namespace std;

main(){
        float pino,cedro,oyamel,superficie;
           
    	cout<<"ingrese la superficie del terreno en metro: ";
        cin>>superficie;
           
        if(superficie>1000000){
        	
            pino=superficie *0.70;
            oyamel=superficie*0.20;
            cedro=superficie *0.10;
            
            }
                           
		else {
            pino=superficie *0.50;
            oyamel=superficie * 0.30;
            cedro=superficie * 0.20;
             }
                                
           cout<<"La superficie de Pino es: " <<pino<<" "<<"metro"<<endl;
           cout<<"La superficie de oyamel es: "<<oyamel<<" "<<"metro"<<endl;
           cout<<" La superficie de Cedro es: "<<cedro<<" "<<"metro"<<endl;
           
}
